﻿using System;
using System.Text.RegularExpressions;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using TechTalk.SpecFlow;
using Xunit;
using yyyeee.Hoist.AcceptanceTests.Pages;

namespace yyyeee.Hoist.AcceptanceTests
{
    [Binding]
    public class LocalWeatherInAGivenCitySteps : IDisposable
    {
        // Next time there should be some base/common place to create driver, also some page factory.
        private readonly ChromeDriver _driver;
        private CurrentWeatherPage _weatherPage;
        private CurrentWeatherPage WeatherPage
        {
            get
            {
                _weatherPage = _weatherPage ?? new CurrentWeatherPage(_driver);
                return _weatherPage;
            }
        }

        public LocalWeatherInAGivenCitySteps()
        {
            _driver = new ChromeDriver
            {
                Url = ConfigurationProvider.ApplicationAddress
            };
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(1);
        }
        public void Dispose()
        {
            _driver.Dispose();
        }

        [Given(@"a webpage with a form")]
        public void GivenAWebpageWithAForm()
        {
            var navigation = new Navigation(_driver);
            navigation.ClickCurrentWeather();

            Assert.NotNull(WeatherPage.Form);
        }

        [Given(@"I type in city '(.*)'")]
        public void GivenITypeInCity(string city)
        {
            WeatherPage.FillCity(city);
        }

        [Given(@"I type in country '(.*)'")]
        public void GivenITypeInCountry(string country)
        {
            WeatherPage.FillCountry(country);
        }

        [When(@"I submit the form")]
        public void WhenISubmitTheForm()
        {
            WeatherPage.SubmitForm();
        }
        
        [Then(@"I receive the temperature and humidity conditions on the day for (.*) according to the official weather reports")]
        public void ThenIReceiveTheTemperatureAndHumidityConditionsOnTheDayForLocationAccordingToTheOfficialWeatherReports(string location)
        {
            var temperatureFieldText = WeatherPage.TemperatureField.Text;
            var humidityFieldText = WeatherPage.HumidityField.Text;

            Assert.True(Regex.IsMatch(temperatureFieldText, @"^[-]?\d+ Celsius$"));
            Assert.True(Regex.IsMatch(humidityFieldText, @"^\d+$"));
            ExpectedConditions.AlertIsPresent().Invoke(_driver);
        }

        [Then(@"I receive the alert that can't check current weather")]
        public void ThenIReceiveTheAlertThatCanTCheckCurrentWeather()
        {
            ExpectedConditions.AlertIsPresent().Invoke(_driver);
        }
    }
}
