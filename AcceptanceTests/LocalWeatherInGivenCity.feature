﻿Feature: Local weather in a given city
	As a delegated employee
	I want to check what the weather is like in a city and country of my choosing
	So that I know how to dress for the business trip over there.

@weather	
Scenario: Checking current weather for city Warsaw
	Given a webpage with a form
	And I type in country 'Poland'
	And I type in city 'Warsaw'
	When I submit the form
	Then I receive the temperature and humidity conditions on the day for 'Warsaw, Poland' according to the official weather reports
	
@weather	
Scenario: Checking current weather for city Gdansk
	Given a webpage with a form
	And I type in country 'Poland'
	And I type in city 'Gdansk'
	When I submit the form
	Then I receive the temperature and humidity conditions on the day for 'Gdansk, Poland' according to the official weather reports

@weather	
Scenario: Checking current weather for city Berlin
	Given a webpage with a form
	And I type in country 'Germany'
	And I type in city 'Berlin'
	When I submit the form
	Then I receive the temperature and humidity conditions on the day for 'Berlin, Germany' according to the official weather reports
	
@weather	
Scenario: Checking current weather for non existing city BlaBla
	Given a webpage with a form
	And I type in country 'BlaBla'
	And I type in city 'BlaBla'
	When I submit the form
	Then I receive the alert that can't check current weather
