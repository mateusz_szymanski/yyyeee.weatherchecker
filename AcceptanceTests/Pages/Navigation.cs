﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace yyyeee.Hoist.AcceptanceTests.Pages
{
    public class Navigation
    {
        [FindsBy(How = How.Id, Using = "currentWeatherNavLink")]
        private IWebElement CurrentWeatherLink { get; set; }

        public Navigation(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void ClickCurrentWeather()
        {
            CurrentWeatherLink.Click();
        }
    }
}