﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace yyyeee.Hoist.AcceptanceTests.Pages
{
    public class CurrentWeatherPage
    {
        [FindsBy(How = How.Id, Using = "currentWeatherForm")]
        public IWebElement Form { get; set; }

        [FindsBy(How = How.Name, Using = "countryInput")]
        private IWebElement CountryInput { get; set; }

        [FindsBy(How = How.Name, Using = "cityInput")]
        private IWebElement CityInput { get; set; }

        [FindsBy(How = How.Id, Using = "checkWeatherButton")]
        private IWebElement CheckButton { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@name='temperatureResult']//div//div//div")]
        public IWebElement TemperatureField { get; set; }

        [FindsBy(How = How.XPath, Using = "//span[@name='humidityResult']//div//div//div")]
        public IWebElement HumidityField { get; set; }

        public CurrentWeatherPage(IWebDriver driver)
        {
            PageFactory.InitElements(driver, this);
        }

        public void FillCountry(string country)
        {
            CountryInput.SendKeys(country);
        }

        public void FillCity(string city)
        {
            CityInput.SendKeys(city);
        }

        public void SubmitForm()
        {
            CheckButton.Click();
        }
    }
}