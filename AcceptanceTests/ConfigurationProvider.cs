﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace yyyeee.Hoist.AcceptanceTests
{
    public static class ConfigurationProvider
    {
        public static string ApplicationAddress => GetConfiguration().GetSection("ApplicationAddress").Value;

        private static IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json");
            var configuration = builder.Build();
            return configuration;
        }
    }
}