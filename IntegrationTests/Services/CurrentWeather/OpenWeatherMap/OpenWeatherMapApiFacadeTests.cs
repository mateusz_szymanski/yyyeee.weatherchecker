﻿using System.Net.Http;
using System.Threading.Tasks;
using SemanticComparison.Fluent;
using Xunit;
using yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap;
using System;
using FluentAssertions;
using NSubstitute.ExceptionExtensions;
using yyyeee.Hoist.Services.CurrentWeather;

namespace yyyeee.Hoist.IntegrationTests.Services.CurrentWeather.OpenWeatherMap
{
    [Trait("Category", "ExternalIntegration")]
    public class OpenWeatherMapApiFacadeTests
    {
        private readonly OpenWeatherMapApiFacade _sut;

        public OpenWeatherMapApiFacadeTests()
        {
            var configOptions =
                ConfigurationHelper.CreateOptions<OpenWeatherMapConfiguration>("Integration:OpenWeatherMap");
            _sut = new OpenWeatherMapApiFacade(configOptions, new HttpClient());
        }

        [Theory]
        [InlineData("pl", "Warsaw")]
        [InlineData("pl", "Gdansk")]
        [InlineData("de", "Berlin")]
        public async Task should_retrieve_data_for_sample_cities(string countryCode, string cityName)
        {
            // Arrange
            var expected = new OpenWeatherMapCurrentWeatherData().AsSource().OfLikeness<OpenWeatherMapCurrentWeatherData>()
                .OmitAutoComparison()
                .With(d => d.Main.Humidity).EqualsWhen((d1, d2) => d2.Main.Humidity.HasValue)
                .With(d => d.Main.Temp).EqualsWhen((d1, d2) => d2.Main.Temp.HasValue);

            // Act
            var actual = await _sut.GetCurrentWeatherDataForLocation(countryCode, cityName);

            // Assert
            expected.ShouldEqual(actual);
        }

        [Theory]
        [InlineData("pl", "SampleCity")]
        [InlineData("SampleCountryCode", "Warsaw")]
        public void should_throw_exception_for_non_existing_city(string countryCode, string cityName)
        {
            // Arrange
            Func<Task> action = async () => await _sut.GetCurrentWeatherDataForLocation(countryCode, cityName);

            // Act & Assert
            action.ShouldThrow<CityNotFoundException>();
        }
    }
}