﻿using System.IO;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using NSubstitute;
using yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap;

namespace yyyeee.Hoist.IntegrationTests
{
    public static class ConfigurationHelper
    {
        public static IOptions<T> CreateOptions<T>(string configSectionName)
            where T : class, new()
        {
            var config = GetSection<T>(configSectionName);
            var configurationOptions = Substitute.For<IOptions<T>>();
            configurationOptions.Value.Returns(config);
            return configurationOptions;
        }

        private static T GetSection<T>(string name)
        {
            var configuration = GetConfiguration();
            var section = configuration.GetSection(name).Get<T>();
            return section;
        }

        private static IConfigurationRoot GetConfiguration()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .AddJsonFile("appsettings.Development.json");
            var configuration = builder.Build();
            return configuration;
        }
    }
}