using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using FluentAssertions;
using FluentAssertions.Json;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json.Linq;
using Xunit;
using yyyeee.Hoist.Web;

namespace yyyeee.Hoist.IntegrationTests.Web.Controllers
{
    public class WeatherControllerGetTests
    {
        private readonly HttpClient _client;

        public WeatherControllerGetTests()
        {
            var server = new TestServer(
                WebHost.CreateDefaultBuilder()
                    .UseStartup<Startup>()
                    .UseEnvironment("IntegrationTests"));
            _client = server.CreateClient();
        }

        [Fact]
        public async Task should_return_weather_for_selected_city()
        {
            // Arrange
            var expected = JToken.Parse(@"
{
    location: {
        city: ""Warsaw"",
        country: ""Poland""
    },
    temperature: {
        format: ""Celsius"",
        value: 16
    },
    humidity: 88
}");

            // Act
            var actual = await _client.GetStringAsync("/api/weather/Poland/Warsaw");

            // Assert
            var actualJson = JToken.Parse(actual);
            actualJson.Should().BeEquivalentTo(expected);
        }

        [Fact]
        public async Task should_return_resource_not_found_when_city_not_found()
        {
            // Act
            var actual = await _client.GetAsync("/api/weather/Poland/SampleCity");

            // Assert
            actual.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task should_return_resource_not_found_when_country_not_found()
        {
            // Act
            var actual = await _client.GetAsync("/api/weather/SampleCountry/Warsaw");

            // Assert
            actual.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NotFound);
        }

        [Fact]
        public async Task should_return_not_found_when_not_all_params_filled()
        {
            // Act
            var actual = await _client.GetAsync("/api/weather/Poland");

            // Assert
            actual.StatusCode.ShouldBeEquivalentTo(HttpStatusCode.NotFound);
        }
    }
}
