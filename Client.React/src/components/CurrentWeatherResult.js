import React from 'react';
import {Card, CardTitle, CardText} from 'material-ui/Card';
import {List, ListItem} from 'material-ui/List';
import {weather} from '../types';

function CurrentWeatherResult({weather}) {
  let shouldShow = weather != null;
  if (shouldShow) {
    const temp = `${weather.temperature.value} ${weather.temperature.format}`;
    return (
      <Card>
        <CardTitle
          title={weather.location.city}
          subtitle={weather.location.country}
        />
        <CardText>
          <List>
            <ListItem secondaryText="Temperature" primaryText={temp} name="temperatureResult"/>
            <ListItem primaryText={weather.humidity} secondaryText="Humidity" name="humidityResult"/>
          </List>
        </CardText>
      </Card>
    );
  }
  return null;
}

CurrentWeatherResult.propTypes = {
  weather: weather
};

export default CurrentWeatherResult;
