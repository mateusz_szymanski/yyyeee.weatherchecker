import React from 'react';
import TextField from 'material-ui/TextField';
import Divider from 'material-ui/Divider';
import RaisedButton from 'material-ui/RaisedButton';
import PropTypes from 'prop-types';
import {location} from '../types';

function CurrentWeatherForm({location, onSubmitClick, onCityChange, onCountryChange}) {
  let canSubmit = location.city != '' && location.country != '';
  return (
    <div id="currentWeatherForm">
      <TextField
        hintText="Type country name..."
        name="countryInput"
        value={location.country}
        onChange={onCountryChange}
        floatingLabelText="Please provide country"
      /><br />
      <TextField
        hintText="Type city name..."
        name="cityInput"
        value={location.city}
        onChange={onCityChange}
        floatingLabelText="Please provide city"
      />
      <Divider />
      
      <RaisedButton
        id="checkWeatherButton"
        label="Check"
        primary={true}
        type="submit" 
        value="Check" 
        onClick={onSubmitClick} 
        disabled={!canSubmit}
      />
    </div>
  );
}

const { func } = PropTypes;

CurrentWeatherForm.propTypes = {
  onSubmitClick: func.isRequired,
  onCityChange: func.isRequired,
  onCountryChange: func.isRequired,
  location: location.isRequired
};

export default CurrentWeatherForm;
