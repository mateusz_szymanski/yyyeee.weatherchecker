import React from 'react';
import {Card, CardTitle, CardText} from 'material-ui/Card';
import { Link } from 'react-router-dom';

const Home = () => {
  return (
    <Card>
      <CardTitle
        title="Current weather"
      />
      <CardText>
        You can check what <Link to="/current-weather">weather</Link> we have around the world!
      </CardText>
    </Card>
  );
};

export default Home;