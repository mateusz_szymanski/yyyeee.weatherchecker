/* eslint-disable import/no-named-as-default */
import React from 'react';
import PropTypes from 'prop-types';
import { Switch, Link, Route } from 'react-router-dom';
import HomePage from './HomePage';
import CurrentWeatherPage from '../containers/CurrentWeatherPage';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';

import {Tabs, Tab} from 'material-ui/Tabs';

class App extends React.Component {
  render() {
    return (
      <MuiThemeProvider>
        <div>
          <Tabs>
            <Tab
              label="Home"
              containerElement={<Link to="/"/>} />
            <Tab
              label="Current weather"
              id="currentWeatherNavLink"
              containerElement={<Link to="/current-weather"/>}/>
          </Tabs>
          <Switch>
            <Route exact path="/" component={HomePage} />
            <Route path="/current-weather" component={CurrentWeatherPage} />
          </Switch>
        </div>
      </MuiThemeProvider>
    );
  }
}

App.propTypes = {
  children: PropTypes.element
};

export default App;
