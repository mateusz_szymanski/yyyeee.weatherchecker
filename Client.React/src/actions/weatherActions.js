import * as types from '../constants/actionTypes';

// Should be done using some config file
/*global __DEV__*/
/*eslint no-undef: "error"*/
const baseApiAddress = __DEV__ ? 'http://localhost:3999/' : '';

export function checkWeather(location) {
  return function (dispatch) {
        fetch(`${baseApiAddress}api/weather/${location.country}/${location.city}`)  
            .then(response => {
                if (response.ok) {
                    return response.json(); 
                } else {
                    if (response.status === 404) {
                        alert('Can\'t find weather for selected city!');
                    } else {
                        alert('Problem occured, please try again!');
                    }
                }
            })
            .then(result => {
                dispatch({
                    type: types.CHECK_WEATHER,
                    weather: result
                });
            })
            .catch(() => {
                alert('Problem occured, please try again!');
            });
  };
}

export function updateLocation(country, city) {
    return {
        type: types.UPDATE_LOCATION,
        location: {
            country: country,
            city: city
        }
    };
}