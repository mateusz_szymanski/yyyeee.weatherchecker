// Centralized propType definitions
import PropTypes from 'prop-types';

const { shape, number, string } = PropTypes;

export const dataState = shape({
  location: location,
  currentWeather: weather
});

export const weather = shape({
  location: location,
  temperature: temperature,
  humidity: number
});

export const temperature = shape({
  value: number,
  format: string
});

export const location = shape({
  country: string,
  city: string
});
