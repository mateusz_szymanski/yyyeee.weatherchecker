import React from 'react';
import {Card, CardTitle, CardText} from 'material-ui/Card';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../actions/weatherActions';
import CurrentWeatherForm from '../components/CurrentWeatherForm';
import CurrentWeatherResult from '../components/CurrentWeatherResult';
import {weather, location} from '../types';

class CurrentWeatherPage extends React.Component{
  checkWeather = () => {
      this.props.actions.checkWeather(this.props.location);
  }

  updateCity = (e) => {
      this.props.actions.updateLocation(this.props.location.country, e.target.value);
  }

  updateCountry = (e) => {
    this.props.actions.updateLocation(e.target.value, this.props.location.city);
  }

  render() {
    return(
        <Card>
          <CardTitle
            title="Check the weather for city"
          />
          <CardText>
            <CurrentWeatherForm 
                onSubmitClick={this.checkWeather} 
                onCountryChange={this.updateCountry}
                onCityChange={this.updateCity}
                location={this.props.location}/>
            <CurrentWeatherResult
                weather={this.props.weather} />
          </CardText>
        </Card>
    );
  }
}

CurrentWeatherPage.propTypes = {
    actions: PropTypes.object.isRequired,
    location: location.isRequired,
    weather: weather,
};

function mapStateToProps(state) {
    return {
        location: state.weather.location,
        weather: state.weather.currentWeather
    };
}
  
function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(actions, dispatch)
    };
}
  
export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CurrentWeatherPage);