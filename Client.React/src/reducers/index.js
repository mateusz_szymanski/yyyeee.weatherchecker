 import { combineReducers } from 'redux';
 import { routerReducer } from 'react-router-redux';
 import weather from './weatherReducer';

 const rootReducer = combineReducers({
    weather,
    routing: routerReducer
  });
  
  export default rootReducer;