import {CHECK_WEATHER, UPDATE_LOCATION} from '../constants/actionTypes';
import objectAssign from 'object-assign';
import initialState from './initialState';

// TODO use immutable.js
export default function weatherReducer(state = initialState.weather, action) {
  let newState;

  switch (action.type) {
    case CHECK_WEATHER:
      newState = objectAssign({}, state);
      newState.currentWeather = action.weather;
      return newState;
      
    case UPDATE_LOCATION:
        newState = objectAssign({}, state);
        newState.location = action.location;
        return newState;

    default:
      return state;
  }
}
