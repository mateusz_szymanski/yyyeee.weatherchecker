# Running server application
## Copy Web/appsettings.json to Web/appsettings.Development.json
### Fill Integration:OpenWeatherMap:ApiKey with value generated from http://openweathermap.org/appid - it is needed to have working API
## Using dotnet CLI (from .NET Core 2.0 SDK)
### Go to Web folder
### Run command: dotnet run 
### You can find API on http://localhost:3999/api

# Running tests
## Integration tests:
### Run command: dotnet test IntegrationTests
## Unit tests:
### Run command: dotnet test Services.Tests
## Acceptance tests:
### Run command: dotnet test AcceptanceTests

# Running client application
## Go to Client.React folder
## Run command: npm install
## Run command: npm start -s
## Go to http://localhost:3000