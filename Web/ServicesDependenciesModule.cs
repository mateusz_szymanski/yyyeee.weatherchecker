﻿using Autofac;
using Microsoft.AspNetCore.Hosting;
using yyyeee.Hoist.Services.Country;
using yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap;

namespace yyyeee.Hoist.Web
{
    public static class ServicesDependenciesModule
    {
        public static void Configure(ContainerBuilder builder, IHostingEnvironment currentEnvironment)
        {
            if (currentEnvironment.IsEnvironment("IntegrationTests"))
            {
                builder.RegisterType<FakeOpenWeatherMapiApiFacade>().AsImplementedInterfaces().InstancePerLifetimeScope();
            }
            else
            {
                builder.RegisterType<OpenWeatherMapApiFacade>().AsImplementedInterfaces();
            }
            builder.RegisterType<OpenWeatherMapCurrentWeatherProvider>().AsImplementedInterfaces();
            builder.RegisterType<CountryProvider>().AsImplementedInterfaces().InstancePerLifetimeScope();
        }
    }
}