﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using yyyeee.Hoist.Services.CurrentWeather;
using yyyeee.Hoist.Services.CurrentWeather.Dtos;

namespace yyyeee.Hoist.Web.Controllers
{
    [Route("api/weather")]
    public class WeatherController : Controller
    {
        private readonly ICurrentWeatherProvider _currentWeatherProvider;

        public WeatherController(ICurrentWeatherProvider currentWeatherProvider)
        {
            _currentWeatherProvider = currentWeatherProvider;
        }

        [HttpGet("{country}/{city}")]
        public async Task<WeatherInfoOutput> Get(CityLocationInput input)
        {
            var weatherInfo = await _currentWeatherProvider.Get(input);
            // Why don't I have mapper here? Because I would use controllers just as an proxy to services,
            // no logic, operations here at all.
            return weatherInfo;
        }
    }
}
