﻿using System.Threading.Tasks;
using yyyeee.Hoist.Services.Country;
using yyyeee.Hoist.Services.CurrentWeather.Dtos;

namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapCurrentWeatherProvider : ICurrentWeatherProvider
    {
        private readonly IOpenWeatherMapApiFacade _apiFacade;
        private readonly ICountryProvider _countryProvider;
        private readonly OpenWeatherMapToWeatherInfoMapper _mapper;

        public OpenWeatherMapCurrentWeatherProvider(IOpenWeatherMapApiFacade apiFacade, ICountryProvider countryProvider)
        {
            _apiFacade = apiFacade;
            _countryProvider = countryProvider;
            _mapper = new OpenWeatherMapToWeatherInfoMapper();
        }

        public async Task<WeatherInfoOutput> Get(CityLocationInput location)
        {
            var countryCode = GetCountryCode(location);
            var data = await GetCurrentWeather(location.City, countryCode);
            var output = _mapper.Map(location, data);
            return output;
        }

        private async Task<OpenWeatherMapCurrentWeatherData> GetCurrentWeather(string city, string countryCode)
        {
            var data = await _apiFacade.GetCurrentWeatherDataForLocation(countryCode, city);
            ValidateData(data);
            return data;
        }

        private string GetCountryCode(CityLocationInput location)
        {
            try
            {
                var code = _countryProvider.GetCodeByName(location.Country);
                return code;
            }
            catch (CountryNotFoundException)
            {
                throw new CityNotFoundException(location.Country, location.City);
            }
        }

        private void ValidateData(OpenWeatherMapCurrentWeatherData data)
        {
            if (data?.Main == null)
            {
                throw new MissingWeatherDataException();
            }
        }

        public class OpenWeatherMapToWeatherInfoMapper
        {
            // TODO Automapper could be used
            public WeatherInfoOutput Map(CityLocationInput location, OpenWeatherMapCurrentWeatherData weatherData)
            {
                var result = new WeatherInfoOutput
                {
                    Humidity = (int?)weatherData.Main?.Humidity,
                    Temperature = new TemperatureInfoOutput
                    {
                        Value = ConvertKelvinToCelsius(weatherData.Main?.Temp),
                        Format = "Celsius"
                    },
                    Location = new LocationInfoOutput
                    {
                        City = location.City,
                        Country = location.Country
                    }
                };
                return result;
            }

            private int? ConvertKelvinToCelsius(decimal? kelvin)
            {
                if (!kelvin.HasValue)
                {
                    return null;
                }
                return (int) (kelvin.Value - (decimal) 273.15);
            }
        }
    }
}