﻿namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapConfiguration
    {
        public string ApiKey { get; set; }
        public string ApiAddress { get; set; }
    }
}