﻿using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapApiFacade : IOpenWeatherMapApiFacade
    {
        private readonly IOptions<OpenWeatherMapConfiguration> _configuration;
        private readonly HttpClient _httpClient;

        public OpenWeatherMapApiFacade(IOptions<OpenWeatherMapConfiguration> configuration, HttpClient httpClient)
        {
            _configuration = configuration;
            _httpClient = httpClient;
        }

        public async Task<OpenWeatherMapCurrentWeatherData> GetCurrentWeatherDataForLocation(string countryCode, string city)
        {
            var url = $"{_configuration.Value.ApiAddress}weather?q={city},{countryCode}&APPID={_configuration.Value.ApiKey}";
            var response = await _httpClient.GetAsync(url);
            EnsureCurrentWeatherResponseSuccesful(countryCode, city, response);
            var stringData = response.Content.ReadAsStringAsync().Result;
            var data = JsonConvert.DeserializeObject<OpenWeatherMapCurrentWeatherData>(stringData);
            return data;
        }

        private static void EnsureCurrentWeatherResponseSuccesful(string countryCode, string city, HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                if (response.StatusCode == HttpStatusCode.NotFound)
                {
                    throw new CityNotFoundException(countryCode, city);
                }
                throw new OpenWeatherMapApiException($"Problem in communication with OpenWeatherMap. Country {countryCode} and city {city}");
            }
        }
    }
}