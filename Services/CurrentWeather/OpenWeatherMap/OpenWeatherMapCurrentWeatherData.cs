﻿using System;

namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapCurrentWeatherData
    {
        public MainData Main { get; set; }

        public class MainData
        {
            public decimal? Temp { get; set; }
            public decimal? Humidity { get; set; }
        }
    }
}