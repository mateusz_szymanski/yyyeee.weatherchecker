﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public class FakeOpenWeatherMapiApiFacade : IOpenWeatherMapApiFacade
    {
        private readonly IDictionary<Tuple<string, string>, OpenWeatherMapCurrentWeatherData> _data = new Dictionary<Tuple<string, string>, OpenWeatherMapCurrentWeatherData>
        {
            { new Tuple<string, string>("Warsaw", "pl"), CreateWeatherData(290, 88)},
            { new Tuple<string, string>("Gdansk", "pl"), CreateWeatherData(280, 90) },
            { new Tuple<string, string>("Berlin", "de"), CreateWeatherData(300, 60) }
        };

        private static OpenWeatherMapCurrentWeatherData CreateWeatherData(decimal temperature, decimal humidity)
        {
            return new OpenWeatherMapCurrentWeatherData
            {
                Main = new OpenWeatherMapCurrentWeatherData.MainData
                {
                    Temp = temperature,
                    Humidity = humidity
                }
            };
        }

        public Task<OpenWeatherMapCurrentWeatherData> GetCurrentWeatherDataForLocation(string countryCode, string city)
        {
            var key = new Tuple<string, string>(city, countryCode);
            if (_data.ContainsKey(key))
            {
                return Task.FromResult(_data[key]);
            }
            throw new CityNotFoundException(countryCode, city);
        }
    }
}