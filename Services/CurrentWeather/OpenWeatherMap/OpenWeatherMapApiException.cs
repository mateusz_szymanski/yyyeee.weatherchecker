﻿using System;

namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapApiException : Exception
    {
        public OpenWeatherMapApiException(string message) : base(message)
        {
        }
    }
}