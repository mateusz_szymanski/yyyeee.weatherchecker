﻿using System.Threading.Tasks;

namespace yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap
{
    public interface IOpenWeatherMapApiFacade
    {
        Task<OpenWeatherMapCurrentWeatherData> GetCurrentWeatherDataForLocation(string countryCode, string city);
    }
}