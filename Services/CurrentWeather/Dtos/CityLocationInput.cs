﻿namespace yyyeee.Hoist.Services.CurrentWeather.Dtos
{
    public class CityLocationInput
    {
        public string Country { get; set; }
        public string City { get; set; }
    }
}
