﻿namespace yyyeee.Hoist.Services.CurrentWeather.Dtos
{
    public class WeatherInfoOutput
    {
        public LocationInfoOutput Location { get; set; }
        public TemperatureInfoOutput Temperature { get; set; }
        public int? Humidity { get; set; }
    }
}