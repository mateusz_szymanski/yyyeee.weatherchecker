﻿namespace yyyeee.Hoist.Services.CurrentWeather.Dtos
{
    public class TemperatureInfoOutput
    {
        public string Format { get; set; }
        public int? Value { get; set; }
    }
}