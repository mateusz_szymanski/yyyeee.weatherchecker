﻿namespace yyyeee.Hoist.Services.CurrentWeather.Dtos
{
    public class LocationInfoOutput
    {
        public string Country { get; set; }
        public string City { get; set; }
    }
}