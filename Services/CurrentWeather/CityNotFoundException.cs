﻿using System;

namespace yyyeee.Hoist.Services.CurrentWeather
{
    public class CityNotFoundException : Exception
    {
        public CityNotFoundException(string country, string city) : base($"Can't find data for country {country} and city {city}")
        {
        }
    }
}