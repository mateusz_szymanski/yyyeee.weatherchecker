﻿using System.Threading.Tasks;
using yyyeee.Hoist.Services.CurrentWeather.Dtos;

namespace yyyeee.Hoist.Services.CurrentWeather
{
    public interface ICurrentWeatherProvider
    {
        Task<WeatherInfoOutput> Get(CityLocationInput location);
    }
}