﻿namespace yyyeee.Hoist.Services.Country
{
    public interface ICountryProvider
    {
        string GetCodeByName(string name);
    }
}