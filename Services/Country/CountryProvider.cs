﻿using System.Collections.Generic;
using System.Linq;
using yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap;

namespace yyyeee.Hoist.Services.Country
{
    // TODO import whole list of country codes
    public class CountryProvider : ICountryProvider
    {
        private readonly List<CountryInfo> _countries = new List<CountryInfo>
        {
            new CountryInfo("pl", "Poland"),
            new CountryInfo("de", "Germany"),
            new CountryInfo("uk", "United Kingdom")
        };
        public string GetCodeByName(string name)
        {
            var country = _countries.FirstOrDefault(c => c.EnglishName == name);
            if (country == null)
            {
                throw new CountryNotFoundException();
            }
            return country.IsoCode;
        }

        private class CountryInfo
        {
            public CountryInfo(string isoCode, string englishName)
            {
                IsoCode = isoCode;
                EnglishName = englishName;
            }

            public string IsoCode { get; set; }
            public string EnglishName { get; set; }
        }
    }
}