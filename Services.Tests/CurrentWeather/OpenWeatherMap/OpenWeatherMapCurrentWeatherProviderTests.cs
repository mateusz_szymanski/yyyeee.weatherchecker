﻿using System;
using System.Threading.Tasks;
using FluentAssertions;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using Ploeh.AutoFixture;
using Xunit;
using yyyeee.Hoist.Services.Country;
using yyyeee.Hoist.Services.CurrentWeather;
using yyyeee.Hoist.Services.CurrentWeather.Dtos;
using yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap;

namespace yyyeee.Hoist.Services.Tests.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapCurrentWeatherProviderTests
    {
        private readonly OpenWeatherMapCurrentWeatherProvider _sut;
        private readonly IOpenWeatherMapApiFacade _apiFacade;
        private readonly ICountryProvider _countryProvider;
        private readonly Fixture _fixture;

        public OpenWeatherMapCurrentWeatherProviderTests()
        {
            _fixture = new Fixture();
            _apiFacade = Substitute.For<IOpenWeatherMapApiFacade>();
            _countryProvider = Substitute.For<ICountryProvider>();
            _sut = new OpenWeatherMapCurrentWeatherProvider(_apiFacade, _countryProvider);
        }

        [Fact]
        public async Task should_return_mapped_result()
        {
            // Arrange
            var location = _fixture.Create<CityLocationInput>();
            var weatherData = _fixture.Create<OpenWeatherMapCurrentWeatherData>();
            var countryCode = _fixture.Create<string>();

            _countryProvider.GetCodeByName(location.Country).Returns(countryCode);
            _apiFacade.GetCurrentWeatherDataForLocation(countryCode, location.City).Returns(Task.FromResult(weatherData));

            var expected = new WeatherInfoOutput
            {
                Humidity = (int?)weatherData.Main.Humidity,
                Location = new LocationInfoOutput
                {
                    Country = location.Country,
                    City = location.City,
                },
                Temperature = new TemperatureInfoOutput
                {
                    Value = (int?)(weatherData.Main.Temp - (decimal) 273.15),
                    Format = "Celsius"
                }
            };

            // Act
            var actual = await _sut.Get(location);

            // Assert
            actual.ShouldBeEquivalentTo(expected);
        }

        [Fact]
        public void should_throw_exception_when_city_not_found()
        {
            // Arrange
            var query = _fixture.Create<CityLocationInput>();
            _apiFacade.GetCurrentWeatherDataForLocation(Arg.Any<string>(), Arg.Any<string>()).Throws(new CityNotFoundException("a", "b"));
            Func<Task> action = async () => await _sut.Get(query);

            // Act & Assert
            action.ShouldThrow<CityNotFoundException>();
        }

        [Fact]
        public void should_throw_exception_when_country_code_not_found()
        {
            // Arrange
            var query = _fixture.Create<CityLocationInput>();
            _countryProvider.GetCodeByName(query.Country).Throws<CountryNotFoundException>();
            Func<Task> action = async () => await _sut.Get(query);

            // Act & Assert
            action.ShouldThrow<CityNotFoundException>();
        }

        [Fact]
        public void should_throw_exception_when_no_data_found()
        {
            _apiFacade.GetCurrentWeatherDataForLocation(Arg.Any<string>(), Arg.Any<string>()).Returns(Task.FromResult((OpenWeatherMapCurrentWeatherData)null));

            TestIfDataNotFoundExceptionIsThrown();
        }

        [Fact]
        public void should_throw_exception_when_no_main_data_found()
        {
            _apiFacade.GetCurrentWeatherDataForLocation(Arg.Any<string>(), Arg.Any<string>()).Returns(Task.FromResult(new OpenWeatherMapCurrentWeatherData()));

            TestIfDataNotFoundExceptionIsThrown();
        }

        private void TestIfDataNotFoundExceptionIsThrown()
        {
            // Arrange
            var query = _fixture.Create<CityLocationInput>();
            Func<Task> action = async () => await _sut.Get(query);

            // Act & Assert
            action.ShouldThrow<MissingWeatherDataException>();
        }
    }
}