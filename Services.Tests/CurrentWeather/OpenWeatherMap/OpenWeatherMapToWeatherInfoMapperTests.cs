﻿using System;
using FluentAssertions;
using Ploeh.AutoFixture;
using Xunit;
using yyyeee.Hoist.Services.CurrentWeather.Dtos;
using yyyeee.Hoist.Services.CurrentWeather.OpenWeatherMap;

namespace yyyeee.Hoist.Services.Tests.CurrentWeather.OpenWeatherMap
{
    public class OpenWeatherMapToWeatherInfoMapperTests
    {
        private readonly OpenWeatherMapCurrentWeatherProvider.OpenWeatherMapToWeatherInfoMapper _sut;
        private readonly Fixture _fixture;

        public OpenWeatherMapToWeatherInfoMapperTests()
        {
            _fixture = new Fixture();
            _sut = new OpenWeatherMapCurrentWeatherProvider.OpenWeatherMapToWeatherInfoMapper();
        }

        [Theory]
        [InlineData(0, -273)]
        [InlineData(273.15, 0)]
        [InlineData(310.15, 37)]
        public void should_convert_kelvin_to_celsius(decimal kelvin, int celsius)
        {
            var expected = new TemperatureInfoOutput
            {
                Value = celsius,
                Format = "Celsius"
            };

            TestTemperature(kelvin, expected);
        }

        [Fact]
        public void should_return_null_when_no_temp()
        {
            // Arrange
            var expected = new TemperatureInfoOutput
            {
                Value = null,
                Format = "Celsius"
            };

            TestTemperature(null, expected);
        }

        private void TestTemperature(decimal? temperature, TemperatureInfoOutput expected)
        {
            // Arrange
            var data = _fixture.Build<OpenWeatherMapCurrentWeatherData>()
                .With(d => d.Main, new OpenWeatherMapCurrentWeatherData.MainData {Temp = temperature}).Create();

            // Act
            var actual = _sut.Map(_fixture.Create<CityLocationInput>(), data);

            // Assert
            actual.Temperature.ShouldBeEquivalentTo(expected);
        }

        [Fact]
        public void should_return_null_when_no_humidity()
        {
            // Arrange
            var data = _fixture.Build<OpenWeatherMapCurrentWeatherData>()
                .With(d => d.Main, new OpenWeatherMapCurrentWeatherData.MainData { Humidity = null }).Create();

            // Act
            var actual = _sut.Map(_fixture.Create<CityLocationInput>(), data);

            // Assert
            actual.Humidity.Should().BeNull();
        }
    }
}